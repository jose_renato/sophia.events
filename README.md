[![pipeline status](https://gitlab.com/lucj/sophia.events/badges/master/pipeline.svg)](https://gitlab.com/lucj/sophia.events/commits/master)

## Purpose

A simple web site, with an easy to remember URL: [http://sophia.events](http://sophia.events) acting as a hub which lists the incoming technical events in Sophia-Antipolis.

## The rational

A lot of great stuff is happening in Sophia but it's not always easy to follow all the sources. Some people ask about a place that lists them all...  here is a simple attempt that tries to answer this need.

Events are regularly added based on content from the following web site :

- [Docker Nice / Sophia-Entipolis meetup](https://www.meetup.com/fr-FR/Docker-Nice/)
- [Sophia-Antipolis Hashicorp User Group](https://www.meetup.com/fr-FR/Sophia-Antipolis-HashiCorp-User-Group/)
- [CNCF Sophia-Antipolis](https://www.meetup.com/fr-FR/CNCF-Cloud-Native-Computing-Sophia-Antipolis/)
- [Développement Mobile Côte d'Azur](https://www.meetup.com/fr-FR/Developpement-Mobile-Android-iOS-Cote-d-Azur/)
- [TelecomValley](http://www.telecom-valley.fr/)
- [RivieraJug](http://rivierajug.org)
- [Softeam group](https://www.softeamgroup.fr/) 
- [Orsys](https://www.orsys.fr/Conferences)

## Want to help making this website look better / cooler ? :)

The project is on [GitLab](https://gitlab.com/lucj/sophia.events).

Fell free to modify it and to submit a [Merge Request](https://gitlab.com/lucj/sophia.events/merge_requests/new)

## Status

- [x] make the whole thing look better
- [ ] make a clean and appealing header
- [ ] add a contact form
- [x] display event logo
- [x] define events.json file describing all the events
- [x] use simple Mustache templating to build index.html
- [ ] Expose RSS feed
- [x] setup a simple CI/CD triggered on PR
- [ ] automate the event retrieval process

## Credit

The first version is based on the vertical [timeline](https://github.com/CodyHouse/vertical-timeline)
